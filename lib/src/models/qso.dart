import 'dart:collection';
import 'dart:core';

import 'package:clock/clock.dart';

import 'storable_object.dart';
import 'time_uid.dart';

enum Band {
  Band160M,
  Band80M,
  Band60M,
  Band40M,
  Band30M,
  Band20M,
  Band17M,
  Band15M,
  Band12M,
  Band10M,
  Band6M,
  Band4M,
  Band2M,
  Band1D25M,
  Band70CM,
  Band33CM,
  Band23CM,
  Band13CM,
}

Band str2Band(String s) =>
    Band.values.firstWhere((e) => e.toString() == 'Band.Band' + s);

String band2Str(Band b) => b.toString().split('.')[1].replaceAll('Band', '');

enum Mode {
  CW,
  SSB,
  FM,
}

Mode str2Mode(String val) =>
    Mode.values.firstWhere((e) => e.toString() == 'Mode.' + val);

String mode2Str(Mode m) => m.toString().split('.')[1];

const Set<Mode> RSTModes = {
  Mode.CW,
};

// ignore: non_constant_identifier_names
final Set<Mode> RSModes = Set.from(Set.from(Mode.values).difference(RSTModes));


class QSO extends StorableObject {
  QSO({
    id,
    timestamp,
    profileId,
    callsign,
    band,
    mode,
    rstRcvd,
    rstSent,
    notesIntl,
    wwff,
    isDeleted,
  }) : super('qsos') {
    this.timestamp = (timestamp ?? DateTime.now()).toUtc();
    this.id = id ?? timeUID();
    this.profileId =
        profileId ?? (throw ArgumentError("profileId is required"));
    this.callsign = callsign ?? (throw ArgumentError("callsign is required"));
    this.band = band ?? (throw ArgumentError("band is required"));
    this.mode = mode ?? (throw ArgumentError("mode is required"));
    this.rstRcvd = rstRcvd ?? (throw ArgumentError("rstRcvd is required"));
    this.rstSent = rstSent ?? (throw ArgumentError("rstSent is required"));
    this.notesIntl = notesIntl ?? '';
    this.wwff = wwff;
    this.isDeleted =
        isDeleted ?? (throw ArgumentError("isDeleted is required"));
  }

  int id;
  DateTime timestamp;
  int profileId;
  String callsign;
  Band band;
  Mode mode;
  String rstRcvd;
  String rstSent;
  String notesIntl;
  String wwff;
  bool isDeleted;

  static String validateRST(Mode mode, String text) {
    if (text == null || text == '') {
      return RSTModes.contains(mode) ? '599' : '59';
    }

    // ignore: non_constant_identifier_names
    final RSRegex = RegExp(r'^[rst]?[1-5][1-9]{0,1}$');
    // ignore: non_constant_identifier_names
    final RSTRegex = RegExp(r'^[rst]?[1-5][1-9]{0,2}$');

    if (!((RSModes.contains(mode) && RSRegex.hasMatch(text)) ||
        (RSTModes.contains(mode) && RSTRegex.hasMatch(text)))) return null;

    text = text.replaceAll(RegExp(r'[rst]'), '') + '99';
    return text.substring(0, RSTModes.contains(mode) ? 3 : 2);
  }

  static String validateWWFFReference(String text) {
    /// If text looks like WWFF reference returns it in "canonical" form.static
    /// Else returns None
    var wwffRefRegex = RegExp(r'^([A-Z0-9]{1,4}FF)-?(\d+)$');

    text = text.toUpperCase();
    Match match = wwffRefRegex.firstMatch(text.toUpperCase());

    if (match == null) return null;
    return match[1] + '-' + match[2].padLeft(4, '0');
  }

  static String validateCallsign(String text) {
    /// A naive check whether text can be a callsign.
    /// If so uppercase text is returned back. Else returns null.

    text = text.toUpperCase();
    // string.asciiUppercase from Python
    final asciiUppercase = Set.from('ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split(''));
    // string.digits from Python
    final digits = Set.from('0123456789'.split(''));

    final chars = Set.from(text.split(''));
    final allowedChars = Set.from({'/'}).union(asciiUppercase).union(digits);

    if (
        // is long enough
        text.length < 3 ||
            // contains not only letters
            chars.difference(asciiUppercase).isEmpty ||
            // contains not only digits
            chars.difference(digits).isEmpty ||
            // contains only allowed characters
            chars.difference(allowedChars).isNotEmpty ||
            // does not contain '//'
            text.contains('//')) return null;

    return text;
  }

  @override
  LinkedHashMap<String, dynamic> toMap() => LinkedHashMap.from({
        'id': id,
        'timestamp': timestamp.toIso8601String(),
        'profileId': profileId,
        'callsign': callsign,
        'band': band2Str(band),
        'mode': mode2Str(mode),
        'rstRcvd': rstRcvd,
        'rstSent': rstSent,
        'notesIntl': notesIntl,
        'wwff': wwff,
        'isDeleted': isDeleted,
      });

  static QSO fromMap(Map<String, dynamic> j) => QSO(
        id: j['id'],
        timestamp: DateTime.parse(j['timestamp']),
        profileId: j['profileId'],
        callsign: j['callsign'],
        band: str2Band(j['band']),
        mode: str2Mode(j['mode']),
        rstRcvd: j['rstRcvd'],
        rstSent: j['rstSent'],
        notesIntl: j['notesIntl'],
        wwff: j['wwff'],
        isDeleted: j['isDeleted'].runtimeType is bool
            ? j['isDeleted']
            : j['isDeleted'] != 0,
      );

  static QSO fromText(String text, int profileId, Band band, Mode mode,
      {clock = const Clock()}) {
    if (text == null || text == '') return null;

    DateTime timestamp = clock.now().toUtc();
    final id = timeUID(dateTime: timestamp);
    String callsign;
    String rstRcvd;
    String rstSent;
    String wwff;
    String notesIntl;

    var notesIntlList = <String>[];

    for (var token in text.trim().split(' ')) {
      if (token == '') continue;

      if (callsign == null) {
        callsign = QSO.validateCallsign(token);
        if (callsign != null) continue;
      }

      if (wwff == null) {
        wwff = QSO.validateWWFFReference(token);
        if (wwff != null) continue;
      }

      if (rstSent == null &&
          token.toLowerCase().startsWith(RegExp(r'^[ts]?[1-5]'))) {
        rstSent = QSO.validateRST(mode, token);
        if (rstSent != null) continue;
      }

      if (rstRcvd == null &&
          token.toLowerCase().startsWith(RegExp(r'^[r]?[1-5]'))) {
        rstRcvd = QSO.validateRST(mode, token);
        if (rstRcvd != null) continue;
      }

      if (token != '') {
        notesIntlList.add(token);
        continue;
      }
    }

    if (callsign == null && notesIntlList.isNotEmpty) {
      callsign = notesIntlList.removeAt(0).toUpperCase();
    }

    rstSent ??= QSO.validateRST(mode, '');
    rstRcvd ??= QSO.validateRST(mode, '');

    notesIntl = notesIntlList.join(' ');

    return QSO(
      id: id,
      timestamp: timestamp,
      profileId: profileId,
      callsign: callsign,
      band: band,
      mode: mode,
      rstRcvd: rstRcvd,
      rstSent: rstSent,
      notesIntl: notesIntl,
      wwff: wwff,
      isDeleted: false,
    );
  }
}
