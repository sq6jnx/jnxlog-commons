import 'package:intl/intl.dart';

int timeUID({DateTime dateTime}) {
  dateTime ??= DateTime.now().toUtc();
  var formatter = DateFormat('yyyyMMddHHmmssSSS');
  var formatted = formatter.format(dateTime);
  return int.parse(formatted) ~/ 10;
}
