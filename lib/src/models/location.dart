import 'dart:collection';

import 'package:meta/meta.dart';
import 'storable_object.dart';

class Location extends StorableObject {
  String program;
  String division;
  String reference;
  String name;
  double latitude;
  double longitude;
  DateTime validFrom;
  DateTime validTo;

  Location({
    @required String program,
    @required String division,
    @required String reference,
    @required String name,
    @required double latitude,
    @required double longitude,
    @required DateTime validFrom,
    DateTime validTo,
  }) : super('locations') {
    this.program = program;
    this.division = division;
    this.reference = reference;
    this.name = name;
    this.latitude = latitude;
    this.longitude = longitude;
    this.validFrom = validFrom;
    this.validTo = validTo;
  }

  Location.fromMap(map)
      : this(
      program: map['program'],
      division: map['division'],
      reference: map['reference'],
      name: map['name'],
      latitude: map['latitude'],
      longitude: map['longitude'],
      validFrom: map['validFrom'],
      validTo: map['validTo']);

  @override
  LinkedHashMap<String, dynamic> toMap() {
    return LinkedHashMap.from({
      'program': program,
      'division': division,
      'reference': reference,
      'name': name,
      'latitude': latitude,
      'longitude': longitude,
      'validFrom': validFrom,
      'validTo': validTo,
    });
  }
}
