import 'package:sqlite3/sqlite3.dart';

import 'migraine.dart';

class Migrations extends Migraine {
  Migrations(Database db) : super(db);

  @override
  List<Function> steps = [
    // @formatter:off
    (Database db) {
      /// Create base tables
      db.execute('''
        CREATE TABLE profiles (
          id INT PRIMARY KEY
          , profileName TEXT NOT NULL DEFAULT ''
          , stationCallsign TEXT NOT NULL DEFAULT ''
          , operator TEXT NOT NULL DEFAULT ''
          , wwffLocation TEXT
          ) WITHOUT ROWID;''');

      db.execute('''
        CREATE TABLE qsos (
          id INT PRIMARY KEY
          , timestamp DATETIME NOT NULL
          , profileId INT NOT NULL REFERENCES profiles (id)
          , callsign TEXT NOT NULL
          , band TEXT NOT NULL
          , mode TEXT NOT NULL
          , rstRcvd TEXT NOT NULL
          , rstSent TEXT NOT NULL
          , notesIntl TEXT NOT NULL DEFAULT ''
          , wwff TEXT
          , isDeleted BOOL NOT NULL DEFAULT FALSE
        ) WITHOUT ROWID;''');

      db.execute('''
        CREATE UNIQUE INDEX qsos__profile_id ON qsos (
          profileId
          , timestamp DESC
          );''');
    },
    (Database db) {
      db.execute('''
      INSERT INTO profiles (id, profileName)
      VALUES (0, 'General profile');''');
    }
    // @formatter:on
  ];
}
