import 'package:sqlite3/sqlite3.dart';

import '../models/profile.dart';
import '../models/qso.dart';
import '../models/storable_object.dart';
import 'migrations.dart';

enum SuggestionMode {
  DUPLICATE,
  SAME_BAND_OR_MODE,
  SAME_PROFILE_AND_DATE,
  SAME_DATE,
  NEW_ONE
}

class CallsignSuggestion {
  String callsign;
  SuggestionMode mode;

  CallsignSuggestion({this.callsign, this.mode});
}

class DB {
  static DB _db;

  factory DB(String location) {
    if (location == null) {
      _db = DB._internal(null);
    } else {
      _db ??= DB._internal(location);
    }

    return _db;
  }

  DB._internal(String location) {
    if (location == null) {
      __database = sqlite3.openInMemory();
    } else {
      __database = sqlite3.open(location);
    }

    var migrations = Migrations(__database);
    migrations.applyPendingMigrations();
  }

  Database __database;

  void _insertOrUpdate(StorableObject o, {bool orReplace = false}) {
    final columns = o.toMap().keys.toList().join(', ');
    final placeholders =
        List.generate(o.toMap().keys.length, (index) => '?').join(', ');
    final sql = '''
      INSERT ${orReplace ? "OR REPLACE" : ""}
      INTO ${o.tableName__} ($columns)
      VALUES ($placeholders);''';
    final statement = __database.prepare(sql);
    var values = o.toMap().values.toList();

    var values_mapped = values.map((e) {
      if (e is bool) {
        return (e == false ? 0 : 1);
      } else {
        return e;
      }
    }).toList();
    statement.execute(values_mapped);
    statement.dispose();
  }

  void insert(StorableObject o) {
    _insertOrUpdate(o);
  }

  void upsert(StorableObject o) {
    _insertOrUpdate(o, orReplace: true);
  }

  Profile getProfile(int id) {
    final sql = 'SELECT * FROM profiles WHERE id = ?';
    final statement = __database.prepare(sql);

    final result = statement.select([id]).toList();
    return result.isNotEmpty ? Profile.fromMap(result.toList()[0]) : null;
  }

  QSO getQso(int id) {
    final sql = 'SELECT * FROM qsos WHERE id = ?';
    final statement = __database.prepare(sql);

    final result = statement.select([id]).toList();
    return result.isNotEmpty ? QSO.fromMap(result.toList()[0]) : null;
  }

  List<QSO> getQsos(int profileId) {
    final sql = 'SELECT * FROM qsos WHERE profileId = ? ORDER BY timestamp DESC';
    final statement = __database.prepare(sql);

    final result = statement.select([profileId]).toList();
    return result.isNotEmpty ? result.map((e) => QSO.fromMap(e)).toList() : [];
  }

  List<Profile> getProfiles() {
    final query = __database.prepare('''
      SELECT DISTINCT
        profiles.*
      FROM
        profiles
        LEFT JOIN qsos ON profiles.id = qsos.profileId
      ORDER BY
        COALESCE(qsos.timestamp, profiles.id) DESC
      ;''');
    final result = query.select();
    return result.map((e) => Profile.fromMap(e)).toList();
  }

  List<CallsignSuggestion> getCallsignSuggestions({
    String callsignPart,
    Band band,
    Mode mode,
    Profile profile,
  }) {
    if (callsignPart == null || callsignPart == '') {
      return [];
    }
    var rv = <CallsignSuggestion>[];
    Set callsignsFound = <String>{};
    final statement = __database.prepare('''
      SELECT
          callsign
          , profileId
          , timestamp
          , band
          , mode
      FROM qsos
      WHERE callsign IN (
          SELECT DISTINCT callsign
          FROM qsos
          WHERE
              callsign LIKE ?
              AND NOT isDeleted
          ORDER BY timestamp DESC
          LIMIT 10
          )
      ORDER BY timestamp DESC
    ;''');

    final result = statement.select(['%$callsignPart%']);
    if (result.isEmpty) return [];
    result.toList().forEach((row) {
      if (callsignsFound.contains(row['callsign'])) {
        return;
      }
      callsignsFound.add(row['callsign']);

      var sameProfile = (profile.id == row['profileId']);
      var sameBand = (band2Str(band) == row['band']);
      var sameMode = (mode2Str(mode) == row['mode']);
      var sameDate = (DateTime.now().toUtc().toString().substring(0, 10) ==
          row['timestamp'].toString().substring(0, 10));

      if (sameProfile && sameBand && sameMode && sameDate) {
        rv.add(CallsignSuggestion(
            callsign: row['callsign'], mode: SuggestionMode.DUPLICATE));
      } else if (sameDate && (sameBand || sameMode) && sameProfile) {
        rv.add(CallsignSuggestion(
            callsign: row['callsign'], mode: SuggestionMode.SAME_BAND_OR_MODE));
      } else if (sameProfile && sameDate) {
        rv.add(CallsignSuggestion(
            callsign: row['callsign'],
            mode: SuggestionMode.SAME_PROFILE_AND_DATE));
      } else if (sameDate) {
        rv.add(CallsignSuggestion(
            callsign: row['callsign'], mode: SuggestionMode.SAME_DATE));
      } else {
        rv.add(CallsignSuggestion(
            callsign: row['callsign'], mode: SuggestionMode.NEW_ONE));
      }
    });
    return rv;
  }
}
