import 'dart:io';

import 'package:jnxLog_commons/models.dart';
import 'package:test/test.dart';

void main() {
  group('timeUid', () {
    test('timeUid gives different results (if some time passed)', () {
      var try1 = timeUID();
      sleep(const Duration(milliseconds: 200));
      var try2 = timeUID();
      expect(try1 < try2, equals(true));
    });
  });
}
