import 'dart:io';

import 'package:clock/clock.dart';
import 'package:jnxLog_commons/models.dart';
import 'package:test/test.dart';

void main() {
  group('RST', () {
    test('Default RST is 59 and 599', () {
      expect(QSO.validateRST(Mode.CW, ''), equals('599'));
      expect(QSO.validateRST(Mode.FM, ''), equals('59'));
      expect(QSO.validateRST(Mode.SSB, ''), equals('59'));
      for (var m in RSTModes) {
        expect(QSO.validateRST(m, null), equals('599'));
      }
      for (var m in RSModes) {
        expect(QSO.validateRST(m, null), equals('59'));
      }
    });

    test('Some strings are not RSTs', () {
      expect(QSO.validateRST(Mode.CW, 'zz'), equals(null));
      expect(QSO.validateRST(Mode.SSB, '7'), equals(null));
      expect(QSO.validateRST(Mode.CW, '599z'), equals(null));
      expect(QSO.validateRST(Mode.CW, '5999'), equals(null));
    });

    test('RSTs are completed with 9s if needed', () {
      expect(QSO.validateRST(Mode.CW, '4'), equals('499'));
      expect(QSO.validateRST(Mode.CW, '59'), equals('599'));
      expect(QSO.validateRST(Mode.CW, '592'), equals('592'));
      expect(QSO.validateRST(Mode.SSB, '47'), equals('47'));
    });
  });

  group('Format WWFF reference', () {
    test('Basc tests', () {
      expect(QSO.validateWWFFReference('spff-0123'), equals('SPFF-0123'));
      expect(QSO.validateWWFFReference('spff1234'), equals('SPFF-1234'));
      expect(QSO.validateWWFFReference('spff2'), equals('SPFF-0002'));
      expect(QSO.validateWWFFReference('fff-1234'), equals('FFF-1234'));
      expect(QSO.validateWWFFReference('fff12345'), equals('FFF-12345'));
      expect(QSO.validateWWFFReference('ff12345'), equals(null));
    });
  });

  group('Validate / return callsign', () {
    test('Basic tests', () {
      expect(QSO.validateCallsign('sq6jnx/p'), equals('SQ6JNX/P'));
      expect(QSO.validateCallsign('sq6jnx'), equals('SQ6JNX'));
      expect(QSO.validateCallsign('m0/sq6jnx/m/p'), equals('M0/SQ6JNX/M/P'));

      expect(QSO.validateCallsign('m0'), equals(null), reason: 'too short');
      expect(QSO.validateCallsign('123'), equals(null), reason: 'only digits');
      expect(QSO.validateCallsign('abc'), equals(null), reason: 'only letters');

      expect(QSO.validateCallsign('c//k0a/m'), equals(null),
          reason: 'double slash');
    });
  });

  group('QSO.fromText()', () {
    test('When no text provided QSO is null', () {
      var qso = QSO.fromText('', 0, Band.Band40M, Mode.CW);
      expect(qso, equals(null));
    });

    test('When there is no match for callsign first notes element is callsign',
        () {
      var qso = QSO.fromText('test', 0, Band.Band40M, Mode.CW);
      expect(qso.callsign, equals('TEST'));
    });

    test('When something looks like RST it must be RST sent', () {
      var qso = QSO.fromText('N0CALL 55', 0, Band.Band40M, Mode.CW);
      expect(qso.callsign, equals('N0CALL'));
      expect(qso.rstSent, equals('559'));
    });

    test('Given two tokens looking like RST first is Sent, other is Received',
        () {
      var qso = QSO.fromText('N0CALL 55 57', 0, Band.Band40M, Mode.CW);
      expect(qso.callsign, equals('N0CALL'));
      expect(qso.rstSent, equals('559'));
      expect(qso.rstRcvd, equals('579'));
    });

    test('RST Sent and Received can be prefixed', () {
      var qso = QSO.fromText('N0CALL t55 r57', 0, Band.Band40M, Mode.CW);
      expect(qso.callsign, equals('N0CALL'));
      expect(qso.rstRcvd, equals('579'));
      expect(qso.rstSent, equals('559'));
    });

    test('Just callsign', () {
      var clock = Clock.fixed(DateTime(2019, 11, 20, 18, 20, 15, 333, 4444));
      var qso = QSO.fromText('sq6jnx', 0, Band.Band40M, Mode.CW, clock: clock);
      expect(qso.callsign, equals('SQ6JNX'));
      expect(qso.id, equals(timeUID(dateTime: clock.now().toUtc())));
      expect(qso.timestamp, equals(clock.now().toUtc()));
      expect(qso.profileId, equals(0));
      expect(qso.callsign, equals('SQ6JNX'));
      expect(qso.band, equals(Band.Band40M));
      expect(qso.mode, equals(Mode.CW));
      expect(qso.rstRcvd, equals('599'));
      expect(qso.rstSent, equals('599'));
      expect(qso.notesIntl, equals(''));
      expect(qso.wwff, equals(null));
      expect(qso.isDeleted, equals(false));
    });
    test('JSON (de-)serialization', () {
      var clock = Clock.fixed(DateTime(2019, 11, 20, 18, 20, 15, 333, 4444));
      var qso1 = QSO.fromMap({
        'id': 2019112018201533,
        'timestamp': clock.now().toIso8601String(),
        'profileId': 0,
        'callsign': 'N0CALL',
        'band': '40M',
        'mode': 'CW',
        'rstRcvd': '599',
        'rstSent': '599',
        'isDeleted': true,
      });
      var qso2 = QSO.fromMap(qso1.toMap());
      expect(qso1.toMap(), equals(qso2.toMap()));
    });
    group('#1 regression: QSO datetime must be UTC', () {
      test('QSO.fromText().timestamp must be UTC', () {
        expect('UTC', QSO
            .fromText('T3ST', 0, Band.Band20M, Mode.FM)
            .timestamp
            .timeZoneName);
      });
      test('QSO.fromMap().timestamp must be UTC', () {
        var qso = QSO.fromText('T3ST', 0, Band.Band20M, Mode.FM);
        expect('UTC', QSO
            .fromMap(qso.toMap())
            .timestamp
            .timeZoneName);
      });
      test('QSO() must be UTC', () {
        var qso = QSO(
            id: 1,
            band: Band.Band20M,
            callsign: 'T3ST',
            isDeleted: false,
            mode: Mode.SSB,
            notesIntl: '',
            profileId: 0,
            rstRcvd: '599',
            rstSent: '599',
            timestamp: DateTime.now(),
            wwff: null);
        expect('UTC', qso.timestamp.timeZoneName);
      });
    });
  });
}
