import 'package:jnxLog_commons/models.dart';
import 'package:test/test.dart';

void main() {
  group('Profile', () {
    test('Profile creation', () {
      var id = 3;
      var profileName = 'Trip to Neverland';
      var stationCallsign = 'N0CALL/p';
      var operator = 'N0CALL';
      var wwff = 'N0FF-1234';

      var p1 = Profile(
        id: id,
        profileName: profileName,
        stationCallsign: stationCallsign,
        operator: operator,
      );
      expect(p1.id, id);
      expect(p1.profileName, profileName);
      expect(p1.stationCallsign, stationCallsign);
      expect(p1.operator, operator);
      expect(p1.wwffLocation, null);

      var p2 = Profile(
          id: id,
          profileName: profileName,
          stationCallsign: stationCallsign,
          operator: operator,
          wwff: wwff);

      expect(p2.id, id);
      expect(p2.profileName, profileName);
      expect(p2.stationCallsign, stationCallsign);
      expect(p2.operator, operator);
      expect(p2.wwffLocation, wwff);
    });

    test('Profile.fromMap().toMap() gives identical Map', () {
      var prototype = {
        'id': 3,
        'profileName': 'Trip to Neverland',
        'stationCallsign': 'N0CALL/p',
        'operator': 'N0CALL',
      };

      var expected = prototype;
      expected.putIfAbsent('wwffLocation', () => null);
      expect(Profile.fromMap(prototype).toMap(), expected);

      expected = prototype;
      expected.putIfAbsent('wwffLocation', () => 'N0FF-1234');
      expect(Profile.fromMap(prototype).toMap(), expected);
    });

    test('Profile can be created given name only', () {
      var profileName = 'Trip to Neverland';
      var p = Profile.fromName(profileName);

      expect(p.toMap().keys,
          ['id', 'profileName', 'stationCallsign', 'operator', 'wwffLocation']);
      expect(p.id - timeUID() < 10, true); // 0.1 sec --> was created right now
      expect(p.profileName, profileName);
      expect(p.stationCallsign, '');
      expect(p.operator, '');
      expect(p.wwffLocation, null);
    });
  });
}
