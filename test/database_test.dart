import 'dart:io';

import 'package:collection/collection.dart';
import 'package:jnxLog_commons/database.dart';
import 'package:jnxLog_commons/models.dart';
import 'package:jnxLog_commons/src/models/qso.dart';
import 'package:quiver/iterables.dart';
import 'package:test/test.dart';

void main() {
  DB db;
  setUp(() async {
    db = DB(null); // create in-memory database
  });

  group('Test database functionalities', () {
    test('When qso does not exist .getQSO returns null', () {
      var qso = db.getQso(12345);
      expect(qso, null);
    });

    test('When no QSO exists in profile .getQsos gives empty list', () {
      db.insert(Profile(
          id: 1,
          operator: 'N0CALL',
          profileName: 'profile',
          stationCallsign: 'N0CALL/P'));
      db.insert(QSO.fromText('N0CALL', 0, Band.Band20M, Mode.FM));
      expect([], db.getQsos(1));
    });

    test('QSOs are fetched in order -- newest first', () {
      // TODO: it looked to be a good idea to use timestamp
      // TODO: to enumerate objects, but now it looks like
      // TODO: it is much harder to perform any tests...
      final qsos = [
        'N0CALL',
        'N1CALL',
        'N2CALL',
      ].map((e) {
        sleep(Duration(milliseconds: 100));
        return QSO.fromText(e, 0, Band.Band20M, Mode.FM);
      }).toList();

      qsos.forEach((element) => db.insert(element));
      zip([qsos.reversed, db.getQsos(0)]).toList().forEach((element) => expect(
          true, MapEquality().equals(element[0].toMap(), element[1].toMap())));
    });

    test('`upsert()` modifies record', () {
      var qso = QSO.fromText('T3ST', 0, Band.Band20M, Mode.FM);
      db.insert(qso);
      qso.isDeleted = true;
      db.upsert(qso);

      qso = db.getQso(qso.id);
      expect(qso.isDeleted, true);
    });
  });

  group('Tests on callsign suggestions', () {
    void assert_suggestions_are_identical(
        List<CallsignSuggestion> cs1, List<CallsignSuggestion> cs2) {
      expect(cs1.length, cs2.length);
      for (var i = 0;
          i < (cs1.length > cs2.length ? cs1.length : cs2.length);
          i++) {
        expect(cs1[i].callsign, cs2[i].callsign);
        expect(cs1[i].mode, cs2[i].mode);
      }
    }

    test('Test on single QSO in database', () {
      var profile = Profile(
          id: 0, profileName: '', stationCallsign: '', operator: '', wwff: '');
      db.insert(QSO(
          profileId: profile.id,
          callsign: 'N0CALL',
          band: Band.Band40M,
          mode: Mode.CW,
          rstRcvd: '599',
          rstSent: '599',
          isDeleted: false));

      assert_suggestions_are_identical(
          db.getCallsignSuggestions(
              callsignPart: '0C',
              band: Band.Band40M,
              mode: Mode.CW,
              profile: profile),
          [
            CallsignSuggestion(
                mode: SuggestionMode.DUPLICATE, callsign: 'N0CALL')
          ]);
      assert_suggestions_are_identical(
          db.getCallsignSuggestions(
              callsignPart: '0C',
              band: Band.Band40M,
              mode: Mode.SSB,
              profile: profile),
          [
            CallsignSuggestion(
                mode: SuggestionMode.SAME_BAND_OR_MODE, callsign: 'N0CALL')
          ]);
      assert_suggestions_are_identical(
          db.getCallsignSuggestions(
              callsignPart: 'SP',
              band: Band.Band40M,
              mode: Mode.SSB,
              profile: profile),
          []);
    });
  });
}
